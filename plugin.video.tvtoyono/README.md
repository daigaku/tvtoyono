# plugin.video.tvtoyono
Kodi Add-on for the youtube channel "Fully Charged" Show about electric cars

![alt text](https://raw.githubusercontent.com/zag2me/plugin.video.tvtoyono/master/icon.png)

**Install**

Launch Kodi >> Add-ons >> Package Icon >> Install from Zip

**Screenshot**

![alt text](https://raw.githubusercontent.com/zag2me/plugin.video.tvtoyono/master/resources/screenshot-01.jpg)
